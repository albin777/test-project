@extends('layouts.app')

@section('content')

    <style>
        .color{
            background-color: #d7d7d7 ;
        }

    </style>

    <table class="table table-bordered users"   >
        <tr class="color">
            <th>icon</th>
            <th>date</th>
            <th>name</th>
            <th>phone</th>
            <th>email</th>
        </tr>


        @foreach($user as $item)
            <tr data-id="{{$item->id}}">

                <td>{{$item->id}} </td>
                <td>{{$item->date}}</td>
                <td>{{$item->name}}</td>
                <td>
                    {{$item->phone->phone}}

                </td>
                <td>{{$item->email}}</td>
            </tr>
        @endforeach
    </table>

    <div class="user-data"></div>


@endsection


<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

<script>
    $(document).ready(function () {
        $('table.users td').click(function () {

            $('.user-data').empty();

            $.get("/user-data/" + $(this).parents('tr').attr('data-id'))
                    .success(function(respond) {

                        $('.user-data')
                                .html('<table class="table table-bordered " style="width: 300px; ">' +

                                        '<tr ><th class="color">icon</th> <th > </th> </tr>' +
                                        '<tr><th class="color">date</th> <th>'+ respond.user.date +'</th> </tr>' +
                                        '<tr><th class="color">name</th> <th>'+ respond.user.name +'</th> </tr>' +
                                        '<tr><th class="color">phone</th>  <th>  {{$item->phone->phone}}   </th> </tr>' +
                                        '<tr><th class="color">email</th> <th> '+ respond.user.email +'</th> </tr>' +
                                        @foreach($SphereAttr as $item)
                                          '<tr><th class="color"> {{$item['label']}} </th> <th>{{$item['options']}} </th> </tr>' +
                                        @endforeach

                                        '</table>');
                    })
                    .error(function() {
                        alert("Ошибка выполнения");
            })

        })
    })
</script>